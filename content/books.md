---
title: "Books"
description: "Kumpulan buku yang sudah gue baca, akan gue baca, dan apa yang lagi gue baca saat ini"
date: 2020-05-31T22:57:06+07:00
cover: /assets/pngs/book.png
draft: false
toc: true
---
I recently got inspiration and wanna read more books from now. If you have any suggestions, feel free to send me a message somewhere!

## Currently reading
---------

| Book                                                                                             | Author       | Date        |
|--------------------------------------------------------------------------------------------------|--------------|-------------|
| [Goodbye, Things: On Minimalist Living](https://goodreads.com/book/show/30231806-goodbye-things) | Fumio Sasaki | 20 Apr 2020 |

## Will read
---------

| Book                                                                                         | Author           |
|----------------------------------------------------------------------------------------------|------------------|
| [Unfu*k Yourself: Get out of your hrad and into your life](https://garyjohnbishop.com/books) | Gary John Bishop |

## Have read
---------

This is by *far* not a complete list; I have read more books than I can possibly remember. This is simply a list of either ones I've read since I started again or the first ones that come to mind when thinking back to when I was younger.

| Book                                                                                                                                                 | Author      | Date        |
|------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|-------------|
| [Atomic Habits: Tiny Changes, Remarkable Results - An Easy & Proven Way to Build Good Habits & Break Bad Ones](https://jamesclear.com/atomic-habits) | James Clear | 10 Apr 2020 |

## Recommendations
---------

These are books and series I have read and highly recommend. I will likely only list one series per author but I encourage you to look more into them and read more of their work. The first few are ranked in order of favouritism but, after that, there is no particular method to my madness.

| Book | Author |
|------|--------|
| None | None   |
