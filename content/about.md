---
title: "About"
date: 2020-04-01T00:00:00+07:00
draft: false
tags:
  - Personal
---
## Hai!

Media ini digunakan untuk cerita gue yaitu "Pembelajaran tiada henti" tetapi gue memilih untuk melakukan apapun yang gue sukai pada media ini jadi gue rasa hal itu tidak terlalu penting.

Gue James Silaban, hanya orang biasa yang diberkati dengan kelebihan bernama "Pantang Menyerah" dan kekurangan bernama "Selalu Kelaparan". Gue punya banyak waktu luang yang gue pakai untuk mempelajari banyak hal seperti Musik, Pemrograman, Privasi & Keamanan, dan masih banyak lagi 😉. Untuk saat ini gue belum mencapai satupun tujuan yang gue inginkan, akan tetapi gue percaya itu sedang dalam proses.
