---
title: "Kegagalan - 73"
description: "Jangan melihat halaman ini! siapa pula yang ingin melihat kegagalan, bukan begitu?"
date: 2020-06-28T22:34:28+07:00
cover: /assets/pngs/failure.png
categories:
  - Edisi Kegagalan
---
Ini yang akan terjadi saat perut lu kelaparan, perasaan lu akan terus murung. Elu harus melihat sesuatu, merasakannya, dan temukan jawaban yang membuat lu yakin
