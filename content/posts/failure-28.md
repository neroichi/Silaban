---
title: "Kegagalan - 28"
description: "Jangan melihat halaman ini! siapa pula yang ingin melihat kegagalan, bukan begitu?"
date: 2020-05-14T20:19:19+07:00
cover: /assets/pngs/failure.png
categories:
  - Edisi Kegagalan
---
Berpura-pura saja untuk berhasil hari ini. Ingin rasanya istirahat sejenak sembari menatap langit di malam hari, apalagi jika disuguhi bintang yang terang benderang.
