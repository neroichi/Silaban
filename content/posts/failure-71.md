---
title: "Kegagalan - 71"
description: "Jangan melihat halaman ini! siapa pula yang ingin melihat kegagalan, bukan begitu?"
date: 2020-06-26T23:59:59+07:00
cover: /assets/pngs/failure.png
categories:
  - Edisi Kegagalan
---
Orang-orang benci pembohong, tetapi pada waktu yang bersamaan orang-orang suka berbohong
