---
title: "What Should I Do?"
description: "This is my first post"
date: 2020-04-15T00:34:23+07:00
cover: /assets/pngs/struggle.png
categories:
  - Piece of Life
layout: "pol"
pol:
  - Struggle
---
Bosan sesaat itu mungkin sudah biasa, tetapi pernahkah lu bingung untuk melakukan sesuatu dan selalu memikirkan hal yang tidak berguna tiap hari, tiap jam, tiap menit, tiap detik, lalu dalam benak lu terlintas "Apa yang telah gue lakukan`sia-sia begitu saja`?"

Sulit untuk memikirkan sesuatu~

Selalu menghela nafas panjang~

Sulit menghadapi`kenyataan`?

Lupa akan sesuatu begitu saja~

Lupa akan janji yang sudah disepakati~

Lupa bahwa ternyata semua hal yang sudah dijalani ternyata`tidak berarti`?

Ada saat dimana gue bisa merasakan penuh bagaimana caranya bernafas, bagaimana caranya berkedip, bagaimana caranya menggerakkan kedua lengan dan kaki, bagaimana caranya menggerakkan jari jemari. Ya! itu sangat luar biasa! Namun tidak dari kita semua bisa`merasakan hal yang sama`, bukan?

Then you ask to me **Am I Okay?**

So, my answer is **What Should I Do?**
