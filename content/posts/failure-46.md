---
title: "Kegagalan - 46"
description: "Jangan melihat halaman ini! siapa pula yang ingin melihat kegagalan, bukan begitu?"
date: 2020-06-01T22:31:19+07:00
cover: /assets/pngs/failure.png
categories:
  - Edisi Kegagalan
---
Gue sekarang menyadari bahwa apa yang dibiasakan akan jadi terbiasa, dan itu berlaku untuk kemalasan yang gue bawa dari dulu sampai sekarang. Tapi gue merasa nikmat dengan kenyamanan ini, walaupun disatu sisi hal itu membuat gue menjadi gila. Yang pasti, sekarang gue lagi lapar. Bye!
