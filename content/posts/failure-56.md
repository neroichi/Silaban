---
title: "Kegagalan - 56"
description: "Jangan melihat halaman ini! siapa pula yang ingin melihat kegagalan, bukan begitu?"
date: 2020-06-11T23:59:59+07:00
cover: /assets/pngs/failure.png
categories:
  - Edisi Kegagalan
---
Seharian ini diri gue gak berguna untuk orang lain. Sebenarnya gue dimintai beberapa pertolongan saat ini, hanya saja keegoisan gue yang menguasai.
