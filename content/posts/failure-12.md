---
title: "Kegagalan - 12"
description: "Jangan melihat halaman ini! siapa pula yang ingin melihat kegagalan, bukan begitu?"
date: 2020-04-28T23:23:23+07:00
cover: /assets/pngs/failure.png
categories:
  - Edisi Kegagalan
---
Melakukan kegagalan itu sungguh melelahkan, percayalah! Sebenarnya gue ingin melakukan sesuatu, akan tetapi hal itu hanya hinggap dipikiran saja dan tidak ada tindakan nyata.
