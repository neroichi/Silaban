---
title: "Jangan Menggunakan Zoom!"
description: "Seharusnya kita tidak menggunakan Zoom, jangan!"
date: 2020-04-16T21:10:14+07:00
cover: /assets/pngs/tech.png
categories:
  - Teknologi
---
Saat ini kita semua sedang menghadapi wabah yang ada diseluruh dunia yaitu pandemi coronavirus, seluruh aktivitas dibatasi dan memaksa kita untuk bekerja dari rumah (Work From Home). Tentu saja bekerja dari rumah itu tidak mudah bagi yang belum terbiasa. Kantor tempat kita bekerja atau dosen di kampus mewajibkan kita melakukan komunikasi lewat video selama bekerja/kuliah bukan?

Zoom[^1] adalah aplikasi yang sedang naik daun ditengah pandemi coronavirus saat ini. Mulai dari Sekolah Menengah Pertama, Sekolah Menengah Atas/Kejuruan, Universitas, dan Kantor menggunakan Zoom sebagai media untuk belajar dan mengerjakan pekerjaan mereka. Tapi haruskah kita menggunakan Zoom? Apakah karena tuntutan kita wajib menggunakannya? Berikut adalah alasan mengapa lu harus meninggalkan Zoom secepatnya!

* Zoom berbohong tentang service mereka yang dilengkapi end-to-end encryption[^2]
* Panggilan Zoom tidak dilengkapi end-to-end encryption
* Zoom mempunyai catatan yang buruk pada celah keamanan mereka
* Anggota panggilan dapat melihat apakah lu sedang fokus atau tidak saat melakukan panggilan
* Zoom menggunakan trik licik untuk mengelabui masyarakat agar dapat mengunduh aplikasi mereka

Dari beberapa alasan diatas sudah tidak ada alasan lagi untuk kita menggunakan zoom, bukan? Akan tetapi, benarkah demikian? Dikutip dari [Techcrunch](https://techcrunch.com/2020/03/31/zoom-at-your-own-risk) bahwa klaim Zoom yang menyesatkan pengguna dalam memberi keamanan dan privasi adalah hal yang salah besar.

Ada kerentanan pada keamanan Zoom untuk Windows yang memungkinkan peretas untuk mencuri password akun lu. Zoom untuk Windows mendukung Universal Naming Convention (UNC) yaitu fitur dimana kita dapat mengkonversi URLs yang dikirimkan menjadi hyperlink. Jadi saat dimana kita meng-klik pada link tersebut akan terbuka tab baru pada browser, dan masalahnya terletak pada bagaimana Zoom menangani URL tersebut. Dikutip dari Security Researcher bernama Mitch (@_g0dmode) di [twitter 23 Maret 2020](https://www.twitter.com/_g0dmode/status/1242131019026874369) yang berbunyi:

>If someone click's on the UNC path URL them Windows will try to establish a connection with the remote site and Windows will send the user's login name and their NLTM password hash, by using the tools like [John the Ripper](https://gbhackers.com/offline-password-attack-john-ripper), Rainbow, Hashcat crack attackers can capture the login credentials.

Mitch berkata bahwa banyak cara peretas untuk mendapatkan hak istimewa sebagai admin pada Windows seperti yang disebutkan diatas. Kemudian ada juga Security Researcher bernama Matthew Hikey (@HackerFantastic) yang mencoba mengeksploit Zoom client pada Windows menggunakan UNC path injection, berikut adalah kutipan dari cuitan Hikey di twitter:

>Hi @zoom_us & @NCSC – here is an example of exploiting the Zoom Windows client using UNC path injection to expose credentials for use in SMBRelay attacks. The screen shot below shows an example UNC path link and the credentials being exposed (redacted). [pic.twitter.com/gjWXas7TMO](https://pic.twitter.com/gjWXas7TMO)

Hikey juga mengatakan bahwa seorang peretas dapat mengeksploitasi UNC path injection untuk menjalankan beberapa kode arbiter pada Windows. Hal ini tidak hanya terjadi pada pengguna Windows saja, berlaku juga untuk pengguna MacOS dan Linux dengan metode yang berbeda. Jika lu ingin mengetahui lebih banyak hal secara teknis mengapa kita harus meninggalkan Zoom, [baca disini](https://citizenlab.ca/2020/04/move-fast-roll-your-own-crypto-a-quick-look-at-the-confidentiality-of-zoom-meetings).

Bagaimana? Apakah lu sudah memutuskan untuk meninggalkan Zoom? Elu bisa mencoba alternatif lain dan jangan lupa pastikan lu membaca *Term of Service* dan *Pricavy Policy* yang mereka berikan 😉

* [Jitsi](https://jitsi.org)
* [Signal](https://signal.org)
* [Jami](https://jami.net)
* [Wire](https://wire.com)
* [Nextcloud Talk](https://nextcloud.com/talk)
* [Matrix](https://matrix.org)
* [Linphone](https://www.linphone.org)

Ingin melakukan panggilan saat bekerja, konferensi video bersama dosen dan teman kampus, menghilangkan rasa rindu bersama pacar, atau sekedar bercakap-cakap dengan anak dan istri. Semua orang berhak mendapatkan hak privasi. [Zoom at your own risk](https://techcrunch.com/2020/03/17/zoombombing)

[^1]: Zoom adalah platform komunikasi berbasis online yang memiliki banyak fitur seperti Video Konferensi, Meeting Online, Chatting, dan Berbagi Konten. [Lihat lebih lengkap](https://www.crunchbase.com/organization/zoom-video-communications)
[^2]: End-to-end Encryption (E2EE) adalah sistem komunikasi antara pihak-pihak terkait yang dilengkapi kunci kriptografi agar tidak ada campur tangan dari pihak ketiga. [End-to-end encryption Wiki](https://en.wikipedia.org/wiki/End-to-end_encryption)
